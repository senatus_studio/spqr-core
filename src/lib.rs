pub mod location;
pub mod time;
pub mod travel;

#[cfg(test)]
mod tests {
    use crate::*;
    use cgmath::Point2;

    #[test]
    fn location_works() {
        let l = location::Location::Coordinate(Point2::new(1., 2.));
        assert_eq!(l.x(), 1.);
        assert_eq!(l.y(), 2.);
    }
}
