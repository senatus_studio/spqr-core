use chrono::{prelude::*, Duration};
use specs::prelude::*;

pub struct DeltaTime {
    pub last: DateTime<Utc>,
    pub now: DateTime<Utc>,
    pub delta: Duration,
}

impl DeltaTime {
    pub fn new() -> Self {
        let last = Utc::now();
        let now = last.clone();
        let delta = Duration::zero();
        DeltaTime { last, now, delta }
    }

    pub fn next_round(&mut self) {
        self.last = self.now;
        self.now = Utc::now();
        self.delta = self.now - self.last;
    }
}

pub struct UpdateTimeSystem;

impl<'a> System<'a> for UpdateTimeSystem {
    type SystemData = Write<'a, DeltaTime>;

    fn run(&mut self, mut time: Self::SystemData) {
        time.next_round();
    }
}

impl Default for DeltaTime {
    fn default() -> Self {
        Self::new()
    }
}
