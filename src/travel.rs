use cgmath::{prelude::*, Vector2};
use specs::{prelude::*, storage::BTreeStorage};
use specs_derive::Component;

use crate::location::{Location, Speed};
use crate::time::DeltaTime;

#[derive(Component, Debug)]
#[storage(BTreeStorage)]
pub struct MoveTarget(Location);

pub struct MoveSystem;

impl<'a> System<'a> for MoveSystem {
    type SystemData = (
        Entities<'a>,
        Read<'a, DeltaTime>,
        WriteStorage<'a, Location>,
        WriteStorage<'a, MoveTarget>,
        ReadStorage<'a, Speed>,
    );

    fn run(&mut self, (ents, time, mut locs, mut tars, spds): Self::SystemData) {
        let mut gets = vec![];

        for (ent, loc, tar, spd) in (&ents, &mut locs, &tars, &spds).join() {
            let dir: Vector2<f32> = tar.0.xy() - loc.xy();
            if dir.magnitude2() >= spd.0.powi(2) {
                *loc = tar.0.clone();
                gets.push(ent);
            } else {
                let dir = dir.normalize() * spd.0 * (time.delta.num_seconds() as f32 / 3600.);
                *loc = Location::Coordinate(loc.xy() + dir);
            }
        }

        gets.into_iter().for_each(|e| {
            tars.remove(e);
        })
    }
}
