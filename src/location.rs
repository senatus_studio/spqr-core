use cgmath::Point2;
use specs::{Component, VecStorage};
use specs_derive::Component;

#[derive(Component, Clone, Debug)]
#[storage(VecStorage)]
pub enum Location {
    Coordinate(Point2<f32>),
}

impl Location {
    pub fn new(x: f32, y: f32) -> Self {
        Location::Coordinate(Point2::new(x, y))
    }

    pub fn x(&self) -> f32 {
        self.xy().x
    }

    pub fn y(&self) -> f32 {
        self.xy().y
    }

    pub fn xy(&self) -> Point2<f32> {
        match self {
            Location::Coordinate(point) => *point,
        }
    }
}

impl From<Location> for Point2<f32> {
    fn from(l: Location) -> Self {
        l.xy()
    }
}

impl From<Point2<f32>> for Location {
    fn from(p: Point2<f32>) -> Self {
        Location::Coordinate(p)
    }
}

#[derive(Component, Debug)]
#[storage(VecStorage)]
pub struct Speed(pub f32);
